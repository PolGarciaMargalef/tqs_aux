
public class Casilla extends Tablero_casilla {

	
	public Casilla(int nfilas, int ncolumnas) {
		super(nfilas, ncolumnas);
		// TODO Auto-generated constructor stub
	}

	public int valor_casella;
	public boolean bomba;
	public boolean recursiva;
	public boolean casella_oberta;
	public boolean bandera;
	
	/*public Casilla() {
		valor_casella=0;
		bomba=false;
		recursiva=true;
		casella_oberta=false;
		bandera=false;
	}*/
	
	public int getValorCasella() {
		return valor_casella;
	}
	
	public boolean getBomba() {
		return bomba;
	}
	
	public boolean getRecursiva() {
		return recursiva;
	}
	
	public boolean getCasellaOberta() {
		return casella_oberta;
	}
	
	public boolean getBandera() {
		return bandera;
	}
	
	//
	
	public void setValorCasilla(int valor_nou) {
		valor_casella=valor_nou;
	}
	
	public void setBomba(boolean hihabomba) {
		bomba= hihabomba;
	}
	
	public void setRecursiva(boolean recursiu) {
		recursiva=recursiu;
	}
	
	public void setCasillaAbierta(boolean casellaOberta) {
		casella_oberta=casellaOberta;
	}
	
	public void setBandera(boolean bandera_marcada) {
		bandera=bandera_marcada;
	}
	
	public void sumaUnValorCasella()
	{
		valor_casella= valor_casella+1;
	}
	
	
	
	
	
}
