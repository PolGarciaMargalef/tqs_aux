//import com.sun.tools.javac.parser.Scanner;


import java.util.*;
import static java.lang.System.out;

public class Tablero_casilla {
	
	public int filas; //Numero de filas
    public int columnas; //Numero de columnas
    public int bombas; //Numero de bombas totales en el juego
    public Casilla[][] tablero;
    

	public int getFilas() {
		return filas;
	}
	
	public int getColumnas() {
		return columnas;
	}
	
	public int getBombas() {
		return bombas;
	}
	
	public void setFilas(int num_filas) {
		filas=num_filas;
	}
	
	public void setColumnas(int num_columnas) {
		columnas=num_columnas;
	}
	
	public void setBombas(int num_bomba) {
		bombas=num_bomba;
	}
    
    //Constructor Tablero Personalizado    
    public Tablero_casilla(int nfilas, int ncolumnas){
        
    	if(nfilas>0) //pedimos un n�mero correcto de filas
    	{
    		filas=nfilas;

    	}
    	else {
    		int auxF=0;
    		while(auxF==0)
    		{	
    			System.out.println("Introduzca n�mero de filas (>0): ");
    			int numeroF = 0;
    			Scanner reader = new Scanner(System.in);
				numeroF = reader.nextInt();
				if(numeroF>0) {
					auxF=1;
					filas=numeroF;
					}
				
    		}
    	}
    	
    	
    	if(ncolumnas>0) //pedimos un n�mero correcto de columnas
    	{
    		filas=ncolumnas;

    	}
    	else {
    		int auxC=0;
    		while(auxC==0)
    		{
    			System.out.println("Introduzca n�mero v�lido de columnas (>0): ");
    			int numeroC = 0;
    			Scanner reader = new Scanner(System.in);
				numeroC = reader.nextInt();
				if(numeroC>0) {
					auxC=1;
					columnas=numeroC;
					}
				
    		}
    	}   	
    	
    	tablero= new Casilla[filas][columnas];  
    	
		Scanner reader1 = new Scanner(System.in);
		int bombas_input = reader1.nextInt();
		
		if(bombas>0 && bombas<(filas*columnas)) //numero correcto de bombas
    	{
    		bombas=bombas_input;
    	}
    	else{
    		int auxB=0;
    		while(auxB==0)
    		{
    			System.out.println("Introduzca n�mero v�lido de bombas (>0 ym�s peque�o de filas*columnas): ");
    			int numeroB = 0;
    			Scanner reader = new Scanner(System.in);
    			bombas_input = reader.nextInt();
				if(bombas_input>0) {
					auxB=1;
					bombas=bombas_input;
				}
		
				inicializarTablero(bombas);	
    		}
    	}
    }
    
    
    
    public void inicializarTablero(int bombas)
    {    	
    	int contadorBombas=0;
    	for(int i=0;i<filas;i++)
    	{
    		for (int j=0;j<columnas;j++)
    		{
    			tablero[i][j].setValorCasilla(0);
    			tablero[i][j].setBomba(false);
    			tablero[i][j].setRecursiva(true);
    			tablero[i][j].setCasillaAbierta(false);
    			tablero[i][j].setBandera(false);
    		}
    	}
    	    	    	   	
    	while(contadorBombas<bombas)
    	{
    		int posicionRandomFila = (int) Math.floor(Math.random()*(filas+1));
    		int posicionRandomColumna = (int) Math.floor(Math.random()*(columnas+1));
        	 
        	 if(tablero[posicionRandomFila][posicionRandomColumna].getBomba()==false)  //Aseguramos que no hay bomba
        	 {
        		 tablero[posicionRandomFila][posicionRandomColumna].setValorCasilla(9);
        		 tablero[posicionRandomFila][posicionRandomColumna].setBomba(true);
        		 tablero[posicionRandomFila][posicionRandomColumna].setRecursiva(false);
        		 contadorBombas++;		////System.out.println(contadorBombas);        		
    		 }   		 
    	}
    	bombasAdyacentes();
    }
    
    
    public void bombasAdyacentes() //Asignamos el # de bombas adyacentes de cada casilla
    {
    	for(int i=0;i<filas;i++)
    	{
    		for (int j=0;j<columnas;j++)
    		{
    			if(tablero[i][j].getBomba()==false) {
    			//Arriba izq
    				if(tablero[i-1][j-1].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    			//Arriba centro
    				if(tablero[i-1][j].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    			//Arriba derecha
    				if(tablero[i-1][j+1].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    				tablero[i][j].sumaUnValorCasella();
    				}
    			//Centro izquierda
    				if(tablero[i][j-1].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    			//Centro derecha
    				if(tablero[i][j+1].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    			//Abajo izquiera
    				if(tablero[i+1][j-1].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    			//Abajo centro
    				if(tablero[i+1][j].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    			//abajo derecha	
    				if(tablero[i+1][j+1].getBomba()==true) {
    					tablero[i][j].setRecursiva(false);
    					tablero[i][j].sumaUnValorCasella();
    				}
    				
    			}	
    		}
    	}
    }
    
    
    //HACER RECURSIVA de 0
    
    public void abrirCasilla0(int fila, int columna) {
		tablero[fila][columna].setCasillaAbierta(true);
		
    	if(tablero[fila-1][columna-1].getValorCasella()==0)
     		abrirCasilla0(fila-1,columna-1);
    	
    	if(tablero[fila-1][columna].getValorCasella()==0)
     		abrirCasilla0(fila-1,columna);
    	
    	if(tablero[fila-1][columna+1].getValorCasella()==0)
     		abrirCasilla0(fila-1,columna+1);
    	
    	if(tablero[fila][columna-1].getValorCasella()==0)
     		abrirCasilla0(fila,columna-1);
    	
    	if(tablero[fila][columna+1].getValorCasella()==0)
     		abrirCasilla0(fila,columna+1);
    	
    	if(tablero[fila+1][columna-1].getValorCasella()==0)
     		abrirCasilla0(fila+1,columna-1);
    	
    	if(tablero[fila+1][columna].getValorCasella()==0)
     		abrirCasilla0(fila+1,columna);
    	
    	if(tablero[fila+1][columna+1].getValorCasella()==0)
     		abrirCasilla0(fila+1,columna+1);    	
    }
    
    
    
    public void abrirCasillaNumero(int fila, int columna) {
		tablero[fila][columna].setCasillaAbierta(true);
		//System.out.println(tablero[fila][columna].getValorCasella());
    }
    
    public void ponerBandera(int fila, int columna) {
    	tablero[fila][columna].setBandera(true);
    }
    
    public void quitarBandera(int fila, int columna) {
    	tablero[fila][columna].setBandera(false);
    }
    
    public void abrirBomba(int fila, int columna) {
    	tablero[fila][columna].setCasillaAbierta(true);
    	System.out.println("FIN PARTIDA"); 
    }
    
    
    public void abrirCasilla(int fila, int columna) {
    	
    	if(tablero[fila][columna].getCasellaOberta()==true) {
    		//casilla
    		System.out.println("Casilla ya abierta, introduzca otra nueva fila y columna:");
    	}
    	else if(tablero[fila][columna].getBomba()==true ) {
    			abrirBomba(fila,columna);
    	}
    	else if(tablero[fila][columna].getValorCasella()==0 && tablero[fila][columna].getBomba()==false) {
    		abrirCasilla0(fila, columna);    		
    	}
    	else if(tablero[fila][columna].getValorCasella()>0 && (tablero[fila][columna].getValorCasella()<9) && tablero[fila][columna].getBomba()==false) {
    		abrirCasillaNumero(fila, columna);
    	}
    	else {
    		System.out.println("�������???????");
    	}
    }
    
    
    public void mostrarTablero() {
    	for(int i=0;i<filas;i++)
    	{
    		for (int j=0;j<columnas;j++)
    		{
    			System.out.println(tablero[i][j].getValorCasella());
    			System.out.println("  ");
    		}
    	System.out.println("\n");
    	}
    }
    
    
    
    public boolean ganarPartida() {
    	int banderasBomba=0;
    	boolean ganar=false;
    	for(int i=0;i<filas;i++)
    	{
    		for (int j=0;j<columnas;j++)
    		{
    			if(tablero[i][j].getBomba()==true && tablero[i][j].getBandera()==true)
    				banderasBomba+=1;
    		}
    	}
    	
    	if(banderasBomba==bombas)
    		ganar=true;
    	return ganar;
    }
}
    
    


